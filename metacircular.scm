(define true #t)
(define false #f)
(define (error msg exp)
  (display "An error happened.") (newline)
  (display "Expression: ") (display exp) (newline)
  (display "Message: ") (display msg) (newline)
  (exit 3))

; ;; This demonstrates why we use dynamic-wind below to isolate state between continuations
; (define global 1)
; 
; (define continuation #f)
; 
; (let ((continuation (call-with-current-continuation
;                      (lambda (jump-out)
;                        (set! global 2)
;                        (call-with-current-continuation jump-out)
;                        (display global))))) ;; should display 2
;   (display global) ;; should display 1
;   (set! global 4)
;   (continuation))

;;; Our plan, however, is to change the type of environment that is passed to eval in order to include
;;; global environment and debug info in it.  The primitive procedures that access it would be changed
;;; to a different type which receives the environment through apply.
;;; (There would be a different kind of apply that receives env as an extra parameter, while the apply
;;;  passed as a primitive to the evaluated scheme would get the environment via the mechanism above.)

(define current-global-environment #f)
(define current-global-debug-info #f)

;; Wrapper around %mc:eval that sets current-global-environment
;; This is passed as eval to the evaluated scheme
(define (mc:eval expr env)
  (let ((get-old-env current-global-environment)
        (get-new-env (lambda () env))
        (get-old-debug current-global-debug-info)
        (get-new-debug (lambda () (make-initial-debug-info))))
    (dynamic-wind
     (lambda ()
       (set! current-global-environment get-new-env)
       (set! current-global-debug-info get-new-debug))
     (lambda ()
       (%mc:eval expr (current-global-environment)))
     (lambda ()
       (set! current-global-environment get-old-env)
       (set! current-global-debug-info get-old-debug)))))

(define (with-debug-info new-info thunk)
  (let* ((get-old-debug current-global-debug-info)
         (get-new-debug (lambda () new-info)))
    (dynamic-wind
     (lambda ()
       (set! current-global-debug-info get-new-debug))
     thunk
     (lambda ()
       (set! current-global-debug-info get-old-debug)))))

(define (make-initial-debug-info)
  (make-debug-info #t '()))

(define (make-debug-info tail-flag continuation-marks)
  (cons tail-flag continuation-marks))

(define (tail-context? info)
  (car info))

(define (get-continuation-marks info)
  (cdr info))

; (define (set-is-tail-call! info is-tail-call)
;   (set-car! info is-tail-call))
; 
; (define (set-continuation-marks! info continuation-marks)
;   (set-cdr! info continuation-marks))
; 
; (define (pop-continuation-mark! info)
;   (set-continuation-marks! info (cdr (get-continuation-marks info))))
; 
; (define (push-continuation-mark! info new-continuation-mark)
;   (set-continuation-marks! info (cons new-continuation-mark (get-continuation-marks info))))

(define (set-is-tail-context info is-tail-context)
  (make-debug-info is-tail-context (get-continuation-marks info)))

(define (push-continuation-mark info new-continuation-mark)
  (make-debug-info (tail-context? info) (cons new-continuation-mark (get-continuation-marks info))))

(define (pop-continuation-mark info)
  (if (null? (get-continuation-marks info))
      info
      (make-debug-info (tail-context? info) (cdr (get-continuation-marks info)))))

(define (%with-is-tail-context is-tail-context thunk)
  (with-debug-info (set-is-tail-context (current-global-debug-info) is-tail-context) thunk))

(define (non-tail-context env proc)
  (%with-is-tail-context #f (lambda () (proc env))))

(define (tail-context env proc)
  (%with-is-tail-context #t (lambda () (proc env))))

(define (%mc:eval exp env)
  (cond ((self-evaluating? exp) exp)
        ((variable? exp) (lookup-variable-value exp env))
        ((quoted? exp) (text-of-quotation exp))
        ((assignment? exp) (eval-assignment exp env))
        ((definition? exp) (eval-definition exp env))
        ((if? exp) (eval-if exp env))
        ((lambda? exp) (make-procedure (lambda-parameters exp)
                                       (lambda-body exp)
                                       env))
        ((begin? exp)
         (eval-sequence (begin-actions exp) env))
        ((cond? exp) (%mc:eval (cond->if exp) env))
        ((and? exp) (%mc:eval (and->if exp) env))
        ((or? exp) (eval-or exp env))
        ((let? exp) (%mc:eval (let->combination exp) env))
        ((let*? exp) (%mc:eval (let*->nested-lets exp) env))
        ((letrec? exp) (%mc:eval (letrec->let-set exp) env))
        ((application? exp)
         (mc:apply (%mc:eval (operator exp) env)
                   (list-of-values (operands exp) env)))
        (else
         (error "Unknown expression type: EVAL" exp))))

(define (mc:apply procedure arguments)
  (cond ((primitive-procedure? procedure)
         (apply-primitive-procedure procedure arguments))
        ((compound-procedure? procedure)
         (tail-context (extend-environment
                        (procedure-parameters procedure)
                        arguments
                        (procedure-environment procedure))
                       (lambda (env)
                         (eval-sequence
                          (procedure-body procedure)
                          env))))
        (else
         (error
          "Unknown procedure type: APPLY" procedure))))

(define (eval-if exp env)
  (if (true? (non-tail-context env (lambda (env) (%mc:eval (if-predicate exp) env))))
      (%mc:eval (if-consequent exp) env)
      (%mc:eval (if-alternative exp) env)))

(define (eval-sequence exps env)
  (cond ((last-exp? exps)
         (%mc:eval (first-exp exps) env))
        (else
         (non-tail-context env
                           (lambda (env)
                             (%mc:eval (first-exp exps) env)))
         (eval-sequence (rest-exps exps) env))))

(define (eval-assignment exp env)
  (set-variable-value! (assignment-variable exp)
                       (%mc:eval (assignment-value exp) env)
                       env)
  'ok)

(define (eval-definition exp env)
  (define-variable! (definition-variable exp)
    (%mc:eval (definition-value exp) env)
    env)
  'ok)

(define (and->if exp)
  (and-exps->if (cdr exp)))

(define (and-exps->if exps)
  (cond ((null? exps) true)
        ((last-exp? exps) (first-exp exps))
        (else
         (make-if (first-exp exps) (and-exps->if (rest-exps exps)) false))))

(define (eval-or-exps exps env)
  (cond ((null? exps) false)
        ((last-exp? exps)
         (%mc:eval (first-exp exps) env))
        (else
         (or (non-tail-context env (lambda (env) (%mc:eval (first-exp exps) env)))
             (eval-or-exps (rest-exps exps) env)))))

(define (eval-or exp env)
  (eval-or-exps (or-expressions exp) env))

(define (self-evaluating? exp)
  (or (number? exp) (string? exp) (boolean? exp)))

(define (variable? exp) (symbol? exp))

(define (quoted? exp) (tagged-list? exp 'quote))
(define (text-of-quotation exp) (cadr exp))

(define (tagged-list? exp tag)
  (and (pair? exp) (eq? (car exp) tag)))

(define (assignment? exp) (tagged-list? exp 'set!))
(define (assignment-variable exp) (cadr exp))
(define (assignment-value exp) (caddr exp))
(define (make-assignment variable value)
  (list 'set! variable value))

(define (definition? exp) (tagged-list? exp 'define))
(define (definition-variable exp)
  (if (symbol? (cadr exp))
      (cadr exp)
      (caadr exp)))
(define (definition-value exp)
  (if (symbol? (cadr exp))
      (caddr exp)
      (make-lambda (cdadr exp)   ; formal parameters
                   (cddr exp)))) ; body

(define (lambda? exp) (tagged-list? exp 'lambda))
(define (lambda-parameters exp) (cadr exp))
(define (lambda-body exp) (cddr exp))

(define (make-lambda parameters body)
  (cons 'lambda (cons parameters body)))

(define (if? exp) (tagged-list? exp 'if))
(define (if-predicate exp) (cadr exp))
(define (if-consequent exp) (caddr exp))
(define (if-alternative exp)
  (if (not (null? (cdddr exp)))
      (cadddr exp)
      'false))

(define (make-if predicate consequent alternative)
  (list 'if predicate consequent alternative))

(define (begin? exp) (tagged-list? exp 'begin))
(define (begin-actions exp) (cdr exp))

(define (last-exp? seq) (null? (cdr seq)))
(define (first-exp seq) (car seq))
(define (rest-exps seq) (cdr seq))

(define (sequence->exp seq)
  (cond ((null? seq) seq)
        ((last-exp? seq) (first-exp seq))
        (else (make-begin seq))))
(define (make-begin seq) (cons 'begin seq))

(define (application? exp) (pair? exp))
(define (operator exp) (car exp))
(define (operands exp) (cdr exp))
(define (no-operands? ops) (null? ops))
(define (first-operand ops) (car ops))
(define (rest-operands ops) (cdr ops))
(define (make-application operator operands) (cons operator operands))

(define (cond? exp) (tagged-list? exp 'cond))
(define (cond-clauses exp) (cdr exp))
(define (cond-else-clause? clause)
  (eq? (cond-predicate clause) 'else))
(define (cond-predicate clause) (car clause))
(define (cond-actions clause) (cdr clause))
(define (cond->if exp) (expand-clauses (cond-clauses exp)))
(define (expand-clauses clauses)
  (if (null? clauses)
      'false ; no else clause
      (let ((first (car clauses))
            (rest (cdr clauses)))
        (if (cond-else-clause? first)
            (if (null? rest)
                (sequence->exp (cond-actions first))
                (error "ELSE clause isn't last: COND->IF"
                       clauses))
            (make-if (cond-predicate first)
                     (sequence->exp (cond-actions first))
                     (expand-clauses rest))))))

(define (and? p) (tagged-list? p 'and))
(define (and-expressions p) (cdr p))

(define (or? p) (tagged-list? p 'or))
(define (or-expressions p) (cdr p))

(define (let? p) (tagged-list? p 'let))
(define (named-let? p) (and (let? p) (symbol? (cadr p))))
(define (named-let-name p) (cadr p))
(define (let-bindings p) (cadr (if (named-let? p) (cdr p) p)))
(define (let-body p) (cddr (if (named-let? p) (cdr p) p)))
(define (let-binding-name p) (car p))
(define (let-binding-value p) (cadr p))
(define (make-let-binding name value) (list name value))
(define (let->combination exp)
  (let ((the-lambda (make-lambda (map let-binding-name (let-bindings exp))
                                 (let-body exp)))
        (the-arguments (map let-binding-value (let-bindings exp))))
    (if (named-let? exp)
        (make-letrec (list (make-let-binding (named-let-name exp) the-lambda))
                     (list (make-application (named-let-name exp) the-arguments)))
        (make-application the-lambda the-arguments))))

(define (make-let bindings body)
  (cons 'let (cons bindings body)))

(define (let*? p) (tagged-list? p 'let*))
(define (let*->nested-lets exp)
  (if (null? (let-bindings exp))
      (make-let '() (let-body exp))
      (make-let (list (car (let-bindings exp)))
                (list (make-let* (cdr (let-bindings exp))
                                 (let-body exp))))))
(define (make-let* bindings body)
  (cons 'let* (cons bindings body)))

(define (letrec? p) (tagged-list? p 'letrec))
(define (letrec->let-set exp)
  (make-let (map (lambda (b) (list (let-binding-name b) #f)) (let-bindings exp))
            (append
             (map (lambda (b) (make-assignment (let-binding-name b) (let-binding-value b)))
                  (let-bindings exp))
             (let-body exp))))
(define (make-letrec bindings body)
  (cons 'letrec (cons bindings body)))

(define (true? x) (not (eq? x false)))
(define (false? x) (eq? x false))

;; The let* is there to ensure evaluation from left to right
;; If it wasn't there, the evaluation order of the underlying scheme would be used.
(define (list-of-values exps env)
  (if (no-operands? exps)
      '()
      (let* ((first (%mc:eval (first-operand exps) env))
             (rest (list-of-values (rest-operands exps) env)))
        (cons first rest))))

(define (make-procedure parameters body env)
  (list 'procedure parameters body env))
(define (compound-procedure? p)
  (tagged-list? p 'procedure))
(define (procedure-parameters p) (cadr p))
(define (procedure-body p) (caddr p))
(define (procedure-environment p) (cadddr p))

(define (enclosing-environment env) (cdr env))
(define (first-frame env) (car env))
(define the-empty-environment '())

(define (make-frame variables values)
  (cons variables values))
(define (frame-variables frame) (car frame))
(define (frame-values frame) (cdr frame))
(define (add-binding-to-frame! var val frame)
  (set-car! frame (cons var (car frame)))
  (set-cdr! frame (cons val (cdr frame))))

(define (extend-environment vars vals base-env)
  (if (= (length vars) (length vals))
      (cons (make-frame vars vals) base-env)
      (if (< (length vars) (length vals))
          (error "Too many arguments supplied" vars vals)
          (error "Too few arguments supplied" vars vals))))

(define (lookup-variable-value var env)
  (define (env-loop env)
    (define (scan vars vals)
      (cond ((null? vars)
             (env-loop (enclosing-environment env)))
            ((eq? var (car vars)) (car vals))
            (else (scan (cdr vars) (cdr vals)))))
    (if (eq? env the-empty-environment)
        (error "Unbound variable" var)
        (let ((frame (first-frame env)))
          (scan (frame-variables frame)
                (frame-values frame)))))
  (env-loop env))

(define (set-variable-value! var val env)
  (define (env-loop env)
    (define (scan vars vals)
      (cond ((null? vars)
             (env-loop (enclosing-environment env)))
            ((eq? var (car vars)) (set-car! vals val))
            (else (scan (cdr vars) (cdr vals)))))
    (if (eq? env the-empty-environment)
        (error "Unbound variable: SET!" var)
        (let ((frame (first-frame env)))
          (scan (frame-variables frame)
                (frame-values frame)))))
  (env-loop env))

(define (define-variable! var val env)
  (let ((frame (first-frame env)))
    (define (scan vars vals)
      (cond ((null? vars)
             (add-binding-to-frame! var val frame))
            ((eq? var (car vars)) (set-car! vals val))
            (else (scan (cdr vars) (cdr vals)))))
    (scan (frame-variables frame) (frame-values frame))))

(define (setup-environment)
  (let ((initial-env
         (extend-environment (primitive-procedure-names)
                             (primitive-procedure-objects)
                             the-empty-environment)))
    (define-variable! 'true true initial-env)
    (define-variable! 'false false initial-env)
    initial-env))

(define (primitive-procedure? proc)
  (tagged-list? proc 'primitive))
(define (primitive-implementation proc) (cadr proc))

(define (make-primitive-procedure proc)
  (list 'primitive proc))

(define (mc:map pred xs)
  (map (lambda (x) (mc:apply pred (list x))) xs))

(define (mc:for-each proc xs)
  (for-each (lambda (x) (mc:apply proc (list x))) xs))

(define (mc:with-input-from-file filename proc)
  (with-input-from-file filename (lambda () (mc:apply proc '()))))

(define (read-s-expressions-from-file filename)
  (with-input-from-file filename
    (lambda ()
      (let loop ()
        (let ((datum (read)))
          (if (eof-object? datum)
              '()
              (cons datum (loop))))))))

(define (mc:load filename)
  (for-each (lambda (s) (%mc:eval s (current-global-environment)))
            (read-s-expressions-from-file filename)))

(define (mc:dynamic-wind before thunk after)
  (dynamic-wind
   (lambda ()
     (mc:apply before '()))
   (lambda ()
     (mc:apply thunk '()))
   (lambda ()
     (mc:apply after '()))))

(define (mc:display s)
  (display 'mc:) (display s))

(define (mc:interaction-environment)
  (current-global-environment))

(define (mc:dbg-tail-call?)
  (tail-context? (current-global-debug-info)))

(define (mc:wcm new-mark thunk)
  (let* ((get-old-debug current-global-debug-info)
         (get-new-debug
          (lambda ()
            (push-continuation-mark
             (if (tail-context? (get-old-debug))
                 (pop-continuation-mark (get-old-debug))
                 (get-old-debug))
             new-mark))))
    (dynamic-wind
     (lambda ()
       (set! current-global-debug-info get-new-debug))
     (lambda ()
       (mc:apply thunk '()))
     (lambda ()
       (set! current-global-debug-info get-old-debug)))))

(define (mc:ccm)
  (get-continuation-marks (current-global-debug-info)))

(define (mc:call-with-current-continuation proc)
  (call-with-current-continuation
   (lambda (cont) (mc:apply proc (list (make-primitive-procedure cont))))))

(define (mc:procedure? obj)
  (or (primitive-procedure? obj) (compound-procedure? obj)))

(define (search-for-procedure procedure-name-as-symbol)
  (eval procedure-name-as-symbol (interaction-environment)))

(define (combinations parts num)
  (let loop ((current-num num)
             (current-combinations '(())))
    (let* ((cons-uncurried-flipped (lambda (xs) (lambda (x) (cons x xs))))
           (multiply (lambda (comb) (map (cons-uncurried-flipped comb) parts))))
      (if (zero? current-num)
          current-combinations
          (loop (- current-num 1)
                (if (= current-num num)
                    (map list parts)
                    (apply append (map multiply current-combinations))))))))

(define (accessors num)
  (define inner-strings
    (map (lambda (s) (apply string-append (map symbol->string s))) (combinations '(a d) num)))
  (map (lambda (s) (string->symbol (string-append "c" s "r"))) inner-strings))

(define primitive-procedures
  (let ((relayed-procedures
         (map (lambda (s) (list s (search-for-procedure s)))
              (append (accessors 1) (accessors 2) (accessors 3) (accessors 4)
                      (list
                       'cons 'null?  'list '= 'length 'eq?  'set-car!
                       'set-cdr!  'number?  'string?  'boolean?  'symbol?  'pair?
                       'newline '+ '- 'read 'eof-object? 'not 'append 'zero? 'string-append
                       'symbol->string 'string->symbol))))
        (prefixed-procedures
         (list
          (list 'map mc:map)
          (list 'apply mc:apply)
          (list 'eval mc:eval)
          (list 'display mc:display)
          (list 'with-input-from-file mc:with-input-from-file)
          (list 'load mc:load)
          (list 'dynamic-wind mc:dynamic-wind)
          (list 'for-each mc:for-each)
          (list 'interaction-environment mc:interaction-environment)
          (list 'dbg-tail-call? mc:dbg-tail-call?) ;; This one was added by us
          (list 'wcm mc:wcm)
          (list 'ccm mc:ccm)
          (list 'call/cc mc:call-with-current-continuation)
          (list 'call-with-current-continuation mc:call-with-current-continuation)
          (list 'procedure? mc:procedure?))))
    (append relayed-procedures prefixed-procedures)))

(define (primitive-procedure-names)
  (map car primitive-procedures))
(define (primitive-procedure-objects)
  (map (lambda (proc) (make-primitive-procedure (cadr proc))) primitive-procedures))

(define (apply-primitive-procedure proc args)
  (apply
   (primitive-implementation proc) args))