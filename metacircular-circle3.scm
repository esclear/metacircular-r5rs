;;; Load the evaluator and let it evaluate metacircular-circle-circle.scm
;;; i.e. evaluate the evaluation of the evaluation of the evaluator definitions using the evaluator using the evaluator
;;; This extra level of meta is useful for ensuring that e.g. load works properly
;;; We didn't run it completely yet, since it takes very long

(load "metacircular.scm")
(define program (read-s-expressions-from-file "metacircular-circle.scm"))
(define the-environment (setup-environment))
(map (lambda (d) (mc:eval d the-environment)) program)