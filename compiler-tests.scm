(load "tests-library.scm")
(load "compiler.scm")

;; TESTS FOR STACK DATA STRUCTURE ;;
; two empty stacks are of different identities
(check (eq? (make-empty-mapping-stack) (make-empty-mapping-stack)) => #f)

(let ((get-new-var-by-key (lambda (k s) (cdr (insert-var-into-stack k s)))))
  (check (equal? (get-new-var-by-key 'some-key (make-empty-mapping-stack))
                 (get-new-var-by-key 'other (make-empty-mapping-stack)))
         => #t)
  (check (symbol? (get-new-var-by-key 'foo (make-empty-mapping-stack))) => #t))

; Thouroughly test stack mechanisms
(let ((s (make-empty-mapping-stack)))
  (check (stack-counter s) => 0)
  (check (find-var 'foo s) => #f)
  (let* ((append  (insert-var-into-stack 'foo s))
         (symbol  (cdr append))
         (astack  (car append))
         (2insert (insert-var-into-stack 'bar astack))
         (2istack (car 2insert))
         (2isymb  (cdr 2insert)))
    (check (stack-counter astack) => 1)
    (check symbol => 'genid_0)
    (check (find-var 'foo astack) => 'genid_0)
    (let* ((pstack (push-new-mapping 2istack)))
      (check (stack-counter pstack) => 2)
      (check (cdr (insert-var-into-stack 'foo pstack)) => 'genid_2)

      (let ((popped_stack (pop-last-mapping 2istack)))
        (check (stack-counter popped_stack) => 2)
        (check (find-var 'foo astack) => 'genid_0)
        ))))

;; TESTS FOR LET TRANSPILING ;;
(let ((some-named-let '(let loop ((a 1)) (cons loop a))))
  (check (let? some-named-let) => #t)
  (check (named-let? some-named-let) => #t))

(let ((id (cdr (insert-var-into-stack 'any-key (make-empty-mapping-stack)))))
  (check (compile '(let ((a 1)) a) (make-default-environment)) (=> equal?) `((lambda (,id) ,id) 1)))

(check (c:eval 1 (make-default-environment)) => 1)
(check (c:eval '(let ((a 1)) 2) (make-default-environment)) => 2)
(check (c:eval '(procedure? (let loop ((a 1)) loop)) (make-default-environment)) => #t)

;; Primitive procedures should map to themselves
(check (c:eval 'procedure? (make-default-environment)) (=> eqv?) procedure?)
(check (c:eval '(procedure? procedure?) (make-default-environment)) => #t)

(check (c:eval '(let loop ((a 1)) (if (= a 1) (loop 2) 'done)) (make-default-environment)) => 'done)

;; TESTS FOR AND TRANSPILING AND EXECUTION ;;
; and
(let ((env (make-default-environment)))
  (check (compile-and '(and) env) => #t)
  (check (compile-and '(and 'foo) env) => ''foo)
  (check (compile-and '(and 'foo 'bar) env) => '(if 'foo 'bar #f))
  (check (compile-and '(and 'foo 'bar 'baz) env) => '(if 'foo (if 'bar 'baz #f) #f))
  (check (c:eval '(and 1 2 3) env) => 3)
  (check (c:eval '(and 1) env) => 1)
  (check (c:eval '(and) env) => #t))

; or
(let ((env (make-default-environment)))
  (check (compile-or '(or) env) => #f)
  (check (compile-or '(or 'foo) env) => ''foo)
  (check (compile-or '(or 'foo 'bar) env) => '(let ((x 'foo)) (if x x 'bar)))
  (check (compile-or '(or 'foo 'bar 'baz) env) => '(let ((x 'foo)) (if x x (let ((x 'bar)) (if x x 'baz)))))
  (check (c:eval '(or 1 2 3) env) => 1)
  (check (c:eval '(or 1) env) => 1)
  (check (c:eval '(or) env) => #f))

(check-report-and-exit)
