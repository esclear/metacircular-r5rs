;;; Load the evaluator and let it evaluate metacircular.scm, i.e. evaluate the evaluator definition using the evaluator

(define filename "metacircular.scm")
(load filename)
(define program (read-s-expressions-from-file filename))
(define the-environment (setup-environment))
(map (lambda (d) (mc:eval d the-environment)) program)