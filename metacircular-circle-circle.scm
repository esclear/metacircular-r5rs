;;; Load the evaluator and let it evaluate metacircular-circle.scm,
;;; i.e. evaluate the evaluation of the evaluator definitions using the evaluator
;;; This extra level of meta is required to make sure we actually evaluate into the expressions,
;;; since not much is evaluated at the time of definition of the evaluator.

(load "metacircular.scm")
(define program (read-s-expressions-from-file "metacircular-circle.scm"))
(define the-environment (setup-environment))
(map (lambda (d) (mc:eval d the-environment)) program)