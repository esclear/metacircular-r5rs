;; See tests-library.scm for detailed information on what you need to run this file.

(load "tests-library.scm")
(load "metacircular.scm")

(define evaluator-program (read-s-expressions-from-file "metacircular.scm"))

;; Evaluate the given expression using mc:eval
(define (evl exp) (mc:eval exp (setup-environment)))

;; Evaluate the evaluator evaluating the given expression using mc:eval using mc:eval
(define (evl2 exp)
  (let ((the-environment (setup-environment)))
    (map (cut mc:eval <> the-environment) evaluator-program)
    (mc:eval `(mc:eval ,exp (setup-environment)) the-environment)))

;;; Tests ;;;

(check (evl '(and #t #f)) => #f)
(check (evl '(and #t #t)) => #t)

(check (evl2 '(let ((a 1)) (= a 1))) => #t)

(check (evl2 '(call/cc (lambda (c) (c 3)))) => 3)
(check (evl2 '(call-with-current-continuation procedure?)) => #t)

(define (dynamic-wind-scenario-prog actually-with-dynamic-wind?)
  `(begin
     (define global 1)
     (define values '())
     (define (add-value! val)
       (set! values (cons val values)))
     
     ,(if actually-with-dynamic-wind?
          '(define (preserving-state thunk)
             (let ((outer-global global)
                   (inner-global global))
               (dynamic-wind
                (lambda ()
                  (set! outer-global global)
                  (set! global inner-global))
                thunk
                (lambda ()
                  (set! inner-global global)
                  (set! global outer-global)))))
          '(define (preserving-state thunk) (thunk)))
     
     (let ((continuation
            (preserving-state
             (lambda ()
               (call-with-current-continuation
                (lambda (jump-out)
                  (set! global 2)
                  (call-with-current-continuation jump-out)
                  (add-value! global)))))))
       (cond ((procedure? continuation)
              (add-value! global)
              (set! global 4)
              (continuation))))
     values))

(check (evl (dynamic-wind-scenario-prog #f)) => '(4 2))
(check (evl (dynamic-wind-scenario-prog #t)) => '(2 1))

(check-report-and-exit)