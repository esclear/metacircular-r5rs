(load "helpers/list.scm")
(load "helpers/error.scm")

(define (c:eval expr env)
  (eval (compile expr env) (get-underlying-scheme-environment env)))

;;; Syntax transformation ;;;

(define (not-implemented-yet? expr)
  (and (list? expr) (memq (car expr) '(letrec))))

(define (compile expr env)
  (cond
   ((not-implemented-yet? expr) expr)
   ((self-compiling? expr) expr)
   ((name? expr) (compile-name expr env))
   ((if? expr) (compile-if expr env))
   ((let? expr) (compile-let expr env))
   ((and? expr) (compile-and expr env))
   ((or? expr) (compile-or expr env))
   ((application? expr) (compile-application expr env))
   (else (error "Unknown expression type" expr))))

(define (compile-sequence seq env)
  (map (lambda (x) (compile x env)) seq))

(define (compile-name expr env)
  (or 
   (find-var expr (get-variable-mappings env))
   (error "Name not bound" expr env)))

(define (compile-if expr env)
  (make-if (compile (if-test expr) env)
	   (compile (if-consequent expr) env)
	   (compile (if-alternative expr) env)))

;; Compiles a given let expression to an application of a lambda.
(define (compile-let expr env)
  (let* ((bind-name
	  (lambda (old-name)
	    (on-variable-mappings!
	     env
	     (lambda (variable-mappings)
	       (insert-var-into-stack old-name variable-mappings)))))
	 (binding->new-name (lambda (binding) (bind-name (let-binding-name binding)))))
    (let ((the-let-name (and (named-let? expr) (bind-name (named-let-name expr))))
	  (the-lambda (make-lambda (map binding->new-name (let-bindings expr)) (compile-sequence (let-body expr) env)))
	  (the-arguments (map let-binding-expr (let-bindings expr))))
      (if (named-let? expr)
	  (compile (make-letrec (list (make-let-binding the-let-name the-lambda))
				(list (make-application the-let-name the-arguments)))
		   env)
	  (make-application the-lambda the-arguments)))))

(define (compile-and expr env)
  (let ((preds (and-predicates expr)))
    (cond
      ((null? preds) #t)
      ((null? (cdr preds)) (car preds))
      (else
       (make-if (car preds) (compile-and `(and ,@(cdr preds)) env) #f)))))

(define (compile-or expr env)
  (let ((preds (or-predicates expr)))
    (cond
      ((null? preds) #f)
      ((null? (cdr preds)) (car preds))
      (else
       (make-let `(x ,(car preds)) `(,(make-if 'x 'x (compile-or `(or ,@(cdr preds)) env)))))
      )))

(define (compile-application expr env)
  (map (lambda (x) (compile x env)) expr))

;;; Syntax accessors ;;;

;;; let construct ;;;

;; Finds out if an expression is of the form `(,tag ...)
(define (tagged-list? exp tag)
  (and (pair? exp) (eq? (car exp) tag)))

(define (quoted? expr) (or (tagged-list? expr 'quasiquote)
			   (tagged-list? expr 'quote)))
(define (self-compiling? expr) (or (boolean? expr) (number? expr) (quoted? expr)))
(define (name? expr) (symbol? expr))

;; Finds out if an expression is a let, including named let
(define (let? exp) (tagged-list? exp 'let))
(define (named-let? exp)
  (and (let? exp) (name? (named-let-name exp))))
(define (named-let-name expr)
  (cadr expr))
(define (let-bindings exp)
  (if (named-let? exp)
      (caddr exp)
      (cadr exp)))
(define (let-body exp)
  (if (named-let? exp)
      (cdddr exp)
      (cddr exp)))

(define (make-let bindings body)
  `(let (,bindings) ,@body))

(define (let-binding-name p) (car p))
(define (let-binding-expr p) (cadr p))
(define (make-let-binding name expr) `(,name ,expr))

(define (make-letrec bindings body)
  `(letrec ,bindings ,@body))

(define (application? expr)
  (list? expr))
(define (make-application proc args)
  (cons proc args))

(define (make-lambda names body)
  `(lambda ,names ,@body))

;;; and expression ;;;
(define (and? exp) (tagged-list? exp 'and))
(define (and-predicates expr) (cdr expr))

;;; if expression ;;;

(define (if? expr) (tagged-list? expr 'if))
(define (if-test expr) (cadr expr))
(define (if-consequent expr) (caddr expr))
(define (if-alternative expr) (cadddr expr))
(define (make-if test consequent alternate)
  `(if ,test ,consequent ,alternate))

;;; or expression ;;;
(define (or? exp) (tagged-list? exp 'or))
(define (or-predicates expr) (cdr expr))


;;;; Data structures ;;;;

;;; Environment ;;:

(define (make-default-environment)
  (make-environment (scheme-report-environment 5) (make-empty-mapping-stack)))

(define (make-environment underlying-scheme-environment variable-mappings)
  (cons underlying-scheme-environment variable-mappings))

(define (get-underlying-scheme-environment env)
  (car env))

(define (get-variable-mappings env)
  (cdr env))

(define (set-variable-mappings! env new-variable-mappings)
  (set-cdr! env new-variable-mappings))

;; Call proc on the variable mappings of env, expecting a pair in return.
;; The car of the pair are set as the new variable mappings into env,
;; and the crd of the pair is returned as the result of on-variable-mappings!.
(define (on-variable-mappings! env proc)
  (let* ((variable-mappings (get-variable-mappings env))
         (proc-result (proc variable-mappings)))
    (set-variable-mappings! env (car proc-result))
    (cdr proc-result)))

; We have two different scenarios for interacting with our variable-mappings:
; 1. When a new variable name is bound in the compiled scheme.
;    This should always create a new unique identifier that will be found
;    nowhere else in the compilation output.
; 2. When a variable name is looked up in the compiled scheme.
;    This should have the ability to fail, so we can report an error if a
;    variable is used in the compiled scheme that wasn't bound.

;; Mapping ;;

(define (%make-empty-mapping)
  (list '*mapping*))

(define (mapping? m)
  (and (pair? m) (eq? (car m) '*mapping*)))

;; Stack of mappings
(define (make-empty-mapping-stack)
  (list '*mapping-stack* 0 (%make-empty-mapping)))

(define (mapping-stack? s)
  (and (pair? s) (eq? (car s) '*mapping-stack*)))

(define (stack-counter stack)
  (cadr stack))

(define (%stack-mappings stack)
  (cddr stack))

(define (%frame-mappings frame)
  (cdr frame))

;; Stack management
(define (insert-var-into-stack key stack)
  (let* ((id          (stack-counter stack))
         (first-frame (car (%stack-mappings stack)))
         (rest        (cdr (%stack-mappings stack)))
         (symbol      (string->symbol (string-append "genid_" (number->string id)))))
    (let ((val (%find-var-by-key key first-frame)))
      (if val
          (cons stack val)
          (cons (%insert-key-var-into-stack key symbol stack) symbol)))))

; TODO: Possibly rethink stack-counter incrementation
(define (%insert-key-var-into-stack key var stack)
  (let* ((id          (stack-counter stack))
         (first-frame (car (%stack-mappings stack)))
         (rest        (cdr (%stack-mappings stack))))
    (apply list `(*mapping-stack* ,(+ id 1) (*mapping* (,key ,var) ,@(%frame-mappings first-frame)) ,@rest))))

(define primitives '(procedure? symbol? =))

(define (primitive? sym) (memq sym primitives))

; Find a key in a mapping
(define (%find-var-by-key key variable-mapping)
  (cond ((assq key (cdr variable-mapping)) => cadr)
        (else #f)))

(define (find-var key mapping-stack)
  (if (primitive? key) key
      (any (lambda (mapping) (%find-var-by-key key mapping)) (%stack-mappings mapping-stack))))

(define (push-new-mapping stack)
  `(*mapping-stack* ,(stack-counter stack) ,(%make-empty-mapping) ,@(%stack-mappings stack)))

(define (pop-last-mapping stack)
  (if (null? (%stack-mappings stack))
      (error "Cannot pop frame from empty stack")
      `(*mapping-stack* ,(stack-counter stack) ,@(cdr (%stack-mappings stack)))))
